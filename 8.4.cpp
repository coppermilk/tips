// 8.4 swaps
#include <iostream>

void swapr(int & a, int & b);
void swapp(int * p, int * q);
void swapv(int a, int b);

int main() {
        using namespace std;
        int wallet1 = 300;
        int wallet2 = 350;
        cout << "wallet1 = $" << wallet1;
        cout << " wallet2 = $" << wallet2 << endl;

        cout << "use links" << endl;
        swapr(wallet1, wallet2);

        cout << "wallet1 = $" << wallet1;
        cout << " wallet2 = $" << wallet2 << endl;

        cout << "use ptr" << endl;
        swapp( & wallet1, & wallet2);

        cout << "wallet1 = $" << wallet1;
        cout << " wallet2 = $" << wallet2 << endl;

        cout << "use simple main" << endl;
        swapv(wallet1, wallet2);

        cout << "wallet1 = $" << wallet1;
        cout << " wallet2 = $" << wallet2 << endl;

        return 0;
}

void swapr(int & a, int & b) {
        int tmp = a;
        a = b;
        b = tmp;
}

void swapp(int * a, int * b) {
        int tmp = * a;
        * a = * b;
        * b = tmp;
}

void swapv(int a, int b) {
        int tmp = a;
        a = b;
        b = tmp;
}
