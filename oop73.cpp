#include<iostream>

using namespace std;

class Human {
        public:
                int age;
        string name;
        int weight;

};

int main() {
        Human firsthuman;
        firsthuman.age = 42;
        firsthuman.name = "Senya";
        firsthuman.weight = 100;

        cout << firsthuman.age << " " << firsthuman.name << "  " <<
                firsthuman.weight << endl;

        Human secondhuman;
        secondhuman.age = 21;
        secondhuman.name = "Dima";
        secondhuman.weight = 65;

        cout << secondhuman.age << " " << secondhuman.name << " " << secondhuman.
        weight << endl;
  
        secondhuman = firsthuman;
  
        cout << secondhuman.age << " " << secondhuman.name << " " << secondhuman.
        weight << endl;
}
