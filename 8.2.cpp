// 8.2 firstref.cpp
#include <iostream>

int main() {
        using namespace std;
        int rats = 101;
        int & rodents = rats;
        
        cout << "rats = " << rats;
        cout << " rodents = " << rodents << endl;
        
        rodents++;
        
        cout << "rats = " << rats;
        cout << " rodents = " << rodents << endl;

        cout << "adress rats = " << & rats;
        cout << " adress rodents = " << & rodents << endl;
        return 0;
}
