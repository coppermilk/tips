/******************************************************************************
8.6 stref.cpp
*******************************************************************************/
#include <iostream>

using namespace std;

struct sysop {
        char name[64];
        char quote[64];
        int used;
};

const sysop & use(sysop & sysopref);

int main() {

        sysop looper = {
                "Рик \'Фортран\' Цикличный",
                "Я выполняю команды",
                0
        };

        use(looper);
        cout << "Looper: " << looper.used << " взовов (a)\n";
        sysop copycat;
        copycat = use(looper);
        cout << "Looper: " << looper.used << " взовов (a)\n";
        cout << "Copycat: " << copycat.used << " взовов (a)\n";
        cout << "use(looper)" << use(looper).used << " взовов (a)\n";
        return 0;
}

const sysop & use(sysop & sysopref) {
        cout << sysopref.name << " Говорит: \n";
        cout << sysopref.quote << endl;
        sysopref.used++;
        return sysopref;
}
