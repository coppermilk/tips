// https://www.youtube.com/watch?v=OYTJ_uOP0zw&list=PLRDzFCPr95fItmofHO4KuGjfGtbQtEj-x&index=9
// my first struct
#include<iostream>

struct Student {
        int age;
        std::string name;
        void init(int _age, std::string _name) {
                this -> name = _name;
                age = _age;
        }
        void aging() {
                age++;
                std::cout << name << " my age is " << age << std::endl;
        }
        void print() const {
                std::cout << name << " - " << age << std::endl;
        }

};

int main() {
        Student a, b;
        a.init(17, std::string("Vasya"));
        b = a;
        a.aging();
        a.print();
        b.print();
}
