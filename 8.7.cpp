#include <iostream>
#include <string>

using namespace std;
string version1(const string & s1,
        const string & s2);
const string & version2(string & s1,
        const string & s2);
const string & version3(const string & s1,
        const string & s2);

int main() {
        string input;
        string copy;
        string result;

        cout << "Input string: ";
        cin >> input;
        copy = input;
        cout << " You input: \"" << input << "\"" << endl;
        result = version1(input, "***");
        cout << "Changed string\"" << result << "\"" << endl;
        cout << " Original input: \"" << input << "\"" << endl;

        result = version2(input, "###");
        cout << "Changed string\"" << result << "\"" << endl;
        cout << " Original input: \"" << input << "\"" << endl;

        input = copy;

        result = version3(input, "@@@");
        cout << "Changed string\"" << result << "\"" << endl;
        cout << " Original input: \"" << input << "\"" << endl;
        return 0;
}
// most straightforward funtion
string version1(const string & s1,
        const string & s2) {
        string temp;
        temp = s2 + s1 + s2;
        return temp;
}

const string & version2(string & s1,
        const string & s2) {
        s1 = s2 + s1 + s2;
        return s1;
}

// bad function
const string & version3(const string & s1,
        const string & s2) {
        string temp;
        temp = s2 + s1 + s2;
        return temp;
}
