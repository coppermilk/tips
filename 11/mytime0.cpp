#include <iostream>
# include "mytime0.h"

Time::Time()
{
	hours = minutes = 0;
}

Time::Time(int h, int m)
{
	hours = h;
	minutes = m;
}



void Time::AddMin(int m) {
	minutes += m;
	hours += minutes / 60;
	minutes %= 60;
}

void Time::AddHr(int h) {
	hours += h;
}

void Time::Reset(int h, int m) {
	hours = h;
	minutes = m;
}

Time Time::Sum(const Time& t) const {
	Time sum;
	sum.minutes = minutes + t.minutes;
	sum.hours = hours + t.hours + sum.minutes / 60;
	sum.minutes %= 60;
	return sum;
}

Time Time::operator+(const Time& t) const
{
	Time tmp;
	tmp.minutes = this->minutes + t.minutes;
	tmp.hours = this->hours + t.hours + tmp.minutes / 60;
	tmp.minutes %= 60;
	return tmp;

}

Time Time::operator-(const Time& t) const
{
	Time diff;
	int tol1, tol2;
	tol1 = t.minutes + 60 * t.hours;
	tol2 = minutes + 60 * hours;
	diff.minutes = (tol2 - tol1) % 60;
	diff.hours = (tol2 - tol1) / 60;
	return diff;

}

Time Time::operator*(double mult) const
{
	Time result;
	long totalmites = hours * mult * 60 + minutes * mult;
	result.hours = totalmites / 60;
	result.minutes = totalmites % 60;
	return result;
}

void Time::Show() const {
	std::cout << hours << " hours, " << minutes << " minutes";
}