
#include <iostream>
#include <string>

using namespace std;
class Point{


private:
    int x;
    int y;
    int z;

public:
    //getter
    int GetX(){return x;}
    int GetY(){return y;}
    
    //setter
    void SetX(int valueX){
        x = valueX;
    }
    void SetY(int valueY){
        y = valueY;
    }
    
    void Print(){
        std::cout << "X = " <<x<< "; Y = " << y << std::endl;
    }
};


int main()
{
    Point p;
    p.SetY(85);
    p.Print();
    
    int result = p.GetX();
    std::cout << result << std::endl;

    return 0;
    //p.z = 32;
}
