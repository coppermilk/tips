// s96.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>
class Pixel {
private:
	int r;
	int b;
	int g;

public:
	Pixel() {
		r = b = g = 0;
	}
	Pixel(int r, int b, int g) {
		this->r = r;
		this->b = b;
		this->g = g;
	}

	std::string getInfo() {
		return "Pixel \tR" + std::to_string(r) + "\tB" + std::to_string(b) + "\tG" + std::to_string(g) + "\n";
	}


};
int main()

{
	const int LIMIT = 5;
	Pixel pixels[LIMIT];
	pixels[0] = Pixel(1, 2, 3);
	std::cout << pixels[0].getInfo();

	Pixel* p_pixels = new Pixel[LIMIT];
	for (int i = 0; i < LIMIT; i++) {
		std::cout << p_pixels[i].getInfo();
	}

	delete[] p_pixels;

}
