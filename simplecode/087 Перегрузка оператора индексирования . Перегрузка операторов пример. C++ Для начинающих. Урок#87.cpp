// s86.cpp 

#include <iostream>

class ArrayClass {
public:
    int & operator [](int index) {
        return arr[index];
    }

private:
    enum{LIMIT = 5};
    int arr[LIMIT] = { 1, 2, 3, 4, 5 };
};


int main() {

    ArrayClass a;
    a[4] = 10;
    std::cout << a[4];
    return 0;
}
