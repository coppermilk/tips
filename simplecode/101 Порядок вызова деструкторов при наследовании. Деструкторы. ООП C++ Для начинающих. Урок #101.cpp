// s99.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class A {
public:
    std::string msg1 = "1";
    A() {
        std::cout << "A constructor is run!" << std::endl;
    }
    ~A() {
        std::cout << "A destructor is run!" << std::endl;
    }
protected:
    std::string msg2 = "2";
private:
    std::string msg3 = "3";
};

class B : public A{
public:
    B() {
        std::cout << "B constructor is run!" << std::endl;
    }
    ~B() {
        std::cout << "B destructor is run!" << std::endl;
    }
    void printMsg() {
  
    msg2 = "this";
    std::cout << msg2;
}

};

class C : public B {
public:
    C() {
        std::cout << "C constructor is run!" << std::endl;
    }
    ~C() {
        std::cout << "C destructor is run!" << std::endl;
    }
};

int main()
{
    C c;
    
}

