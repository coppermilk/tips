#include <iostream>
#include <string>

using namespace std;

class Point{
private:
    int x;
    int y;

public:

Point(int valueX, int valueY){ // <----------------- constructor
    x = valueX;
    y = valueY;
}
    //getter
    int GetX(){return x;}
    int GetY(){return y;}
    
    //setter
    void SetX(int valueX){
        x = valueX;
    }
    void Set(int valueY){
        y = valueY;
    }
    
    void Print(){
        std::cout << "X = " <<x<< "; Y = " << y << std::endl;
    }
};



int main()
{ 
    Point p(5, 24);
    Point b(14, 96);
    p.Print();
    b.Print();
    
    p = b;
    p.Print();
    return 0;
    //p.z = 32;
}
