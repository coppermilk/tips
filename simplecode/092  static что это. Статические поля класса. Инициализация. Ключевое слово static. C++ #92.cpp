// s92.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

class Apple {
public:
    static int count;
    Apple(int weight, std::string color) {
        this->weight = weight;
        this->color = color;
        count++;
    }
private:
    int weight;
    std::string color;
};

int Apple::count = 0;

int main()
{
    Apple a(150, "red");
    Apple b(150, "red");
    Apple c(150, "red");
    std::cout << Apple::count;
    std::cout << "Hello World!\n";
}

