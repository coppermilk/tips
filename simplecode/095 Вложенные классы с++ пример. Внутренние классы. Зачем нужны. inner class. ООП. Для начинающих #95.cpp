// s95.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

class Image {
public:
	void GetImageInfo() {
		for (int i = 0; i < 5; i++) {
			std::cout << "#" << i << " " << pixels[i].getInfo();
		}
	}

private:
	class Pixel {
	private:
		int r;
		int b;
		int g;

	public:
		Pixel(int r, int b, int g) {
			this->r = r;
			this->b = b;
			this->g = g;
		}
		std::string getInfo() {
			return "Pixel \tR" + std::to_string(r) + "\tB" + std::to_string(b) + "\tG" + std::to_string(g) + "\n";
		}


	};
public:
	Image::Pixel pixels[5]{
		Pixel(0, 4, 684),
		Pixel(1, 44, 864),
		Pixel(2, 44, 64),
		Pixel(3, 64, 634),
		Pixel(4, 84, 64),
	};


};

int main()
{
	Image img;
	
	img.GetImageInfo();

}
