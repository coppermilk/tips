
#include <iostream>
using namespace std;
void foo(int i);

class MyClass{
    private:
        int* data;
    public:
    MyClass(int size){
        data = new int[size]; // <-----------------
        for (int i = 0; i < size; i++){
            data[i] = i;
        }
        cout << "Вызвался конструктор" << data << endl;
    }
    ~MyClass(){
       cout << "Вызвался деструктор"<< data << endl;
       delete [] data; // <-----------------
    }
};
void foo(int i){
    cout << "start"<< endl;
    MyClass q(i);
    cout << "end" << endl;
}
int main()
{
     MyClass m(5);
     MyClass a(2);
     foo(42);
     

    return 0;
}
