// s90.cpp

#include <iostream>
#include <string>
using namespace std;

class Human;
class Apple;

class Human {
public:
    void TakeApple(Apple& apple);
    void EatApple(Apple& apple) {

    }
};

class Apple {
private:
    int weight;
    std::string color;
public:
    Apple(int weight, std::string color) {
        this->weight = weight;
        this->color = color;
    }
    friend void Human::TakeApple(Apple& apple); // <------ так как функция дружественная она может получить доступ к privat
    // внее класса
};



int main()
{
    Apple apple(150, "Red");
    Human h;
    h.TakeApple(apple);
}

void Human::TakeApple(Apple& apple) // <------ вне класса
{
std:cout << "Take apple " << apple.weight <<" gramm" << endl;
}
