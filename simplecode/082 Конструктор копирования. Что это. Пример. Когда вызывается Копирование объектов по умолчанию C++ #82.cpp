
#include <iostream>
using namespace std;
class MyClass{
    public:
    int *data;
    
    MyClass(int size){
        this->Size = size;
        this->data = new int[size];
        for (int i = 0; i < size; i++){
            data[i] = i;
        }
        cout << "constructor " << this << endl;
    }
    
    MyClass(const MyClass &other){ //<-----------------Конструктор копирование
        this-> Size = other.Size;
        cout << "copy " << this << endl;
        
        this->data = new int[other.Size];
        for(int i = 0; i < other.Size; i++){
            this->data[i] = other.data[i];
        }
    }
    
    ~MyClass(){
        cout << "destructor" << this << endl;
        delete [] data;
    }
    
    private:
        int Size;
};

void Foo(MyClass value){
     cout << "foo function" << endl;
}

MyClass Foo2(){
    MyClass temp(2);
    cout << "foo2 function" << endl;
    return temp;
    
}

int main()
{
    MyClass c(10);
    MyClass b(c);
    //Foo(c);
    //Foo2();

    return 0;
}

