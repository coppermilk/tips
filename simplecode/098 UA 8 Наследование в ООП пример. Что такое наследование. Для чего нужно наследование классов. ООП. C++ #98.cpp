// s98.cpp 
#include <iostream>

class Human {
private:
    std::string name = "Evgeniy";
public:
    std::string getName() {
        return name;
    }
    void setName(std::string name) {
        this->name = name;
    }

};

class Student : public Human { //<------------ public class
private:
    
public:
    std::string group;
    void learn() {
        std::cout << "I learn!" << std::endl;
    }
};

class ExtramuralStudent : public Student { //<------------ public class
private:
public:
    void learn() {
        std::cout << "I learn at home!" << std::endl;
    }
};

class Professor : public Human {
private:
    
public:
    std::string subject;

};
int main()

{
    Student s;
    s.group;
    s.setName("Maestro");
    s.learn();
    ExtramuralStudent es;
    es.learn();
    es.setName("Evgeniy");
    std::cout << es.getName() << std::endl;
    
}
