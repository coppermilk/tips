// 94
#include <iostream>
#include <string>

class Apple {
public:
    static int count;
    int getId() { return id; }
    Apple(int weight, std::string color) {
        this->weight = weight;
        this->color = color;
        count++;
        id = count;
    }
    static int getCount() {
        return count;
    }
    static void changeColor(Apple& a, std::string color) {
        a.color = color;
    }
private:

    int id;
    int weight;
    std::string color;
};

int Apple::count = 0;

int main()
{
    Apple a(150, "red");
    Apple b(150, "red");
    Apple::changeColor(b, "green");
    Apple c(150, "red");
    std::cout << Apple::getCount() << std::endl;
    std::cout << a.getId() << std::endl;
    std::cout << b.getId() << std::endl;
    std::cout << c.getId() << std::endl;
    std::cout << "Hello World!\n";
}

