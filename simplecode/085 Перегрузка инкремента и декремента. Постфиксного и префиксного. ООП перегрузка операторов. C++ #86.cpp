// s86.cpp 

#include <iostream>

class Point {
private:
    int x;
    int y;

public:

    Point() { // <-----------constructor 1
        x = 0;
        y = 0;
    }

    Point(int valueX, int valueY) { // <-----------constructor 2
        x = valueX;
        y = valueY;
    }

    Point(int valueX, bool bollean) {// <-----------constructor 3
        x = valueX;
        if (bollean) {
            y = 42;
        }
        else {
            y = -95;
        }
    }
    //getter
    int GetX() { return x; }
    int GetY() { return y; }

    //setter
    void SetX(int valueX) {
        x = valueX;
    }
    void SetY(int y) {
        this->y = y; //<-------------- приминение ключевого слово this
    }

    void Print() {
        std::cout << "constructor " << this << " X = " << x << "; Y = " << y << std::endl;
    }

    bool operator == (const Point& other) {
        return ((other.x == this->x) && (other.y == this->y));

    }

    bool operator != (const Point& other) {
        return !((other.x == this->x) && (other.y == this->y));

    }
    Point & operator ++ () {
        this->x++;
        this->y++;
        return *this;
       
    }

    Point & operator ++ (int value) {
        Point temp(*this);

        this->x++;
        this->y++;

        return temp;
    }

};


int main()
{
    Point p(2, 3);
    
 
    Point b = p++;
    b.Print();
    p.Print();

}

