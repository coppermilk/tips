// s97.cpp 

#include <iostream>
#include <string>

class Cap {
public:
    std::string getColor() { return color; }
private:
    std::string color = "Blue";
    // <----- агрегация жестко не привязяна к human
};

class Model {
public:
    void InspectTheCap() {
        std::cout << "My cap is " << cap.getColor() << std::endl;
    }
private:
    Cap cap;
};

class Human {
public:
    void Think() {
        brain.Think();
    }

    void InspectTheCap() {
        std::cout << "My cap is " << cap.getColor() << std::endl;
    }
private:
    class Brain {
    public:
        void Think() {
            std::cout << "I THINK" << std::endl;
        }
    };
    Brain brain;
    Cap cap;

    
};

int main()
{
    Human human;
    human.Think(); // <----------- composition
    human.InspectTheCap();
    Model model;
    model.InspectTheCap();// <----------- composition

}

