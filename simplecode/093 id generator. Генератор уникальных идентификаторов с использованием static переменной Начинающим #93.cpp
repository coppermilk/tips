// s93.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <string>

class Apple {
public:
    static int count;
    int getId() { return id; }
    Apple(int weight, std::string color) {
        this->weight = weight;
        this->color = color;
        count++;
        id = count;
    }
private:
    int id;
    int weight;
    std::string color;
};

int Apple::count = 0;

int main()
{
    Apple a(150, "red");
    Apple b(150, "red");
    Apple c(150, "red");
    std::cout << Apple::count << std::endl;
    std::cout << a.getId() << std::endl;
    std::cout << b.getId() << std::endl;
    std::cout << c.getId() << std::endl;
    std::cout << "Hello World!\n";
}

