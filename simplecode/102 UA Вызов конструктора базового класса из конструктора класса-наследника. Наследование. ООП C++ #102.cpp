// s99.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class A {
public:
    std::string msg1 = "1";
    A() {
        std::cout << "A constructor is run!" << std::endl;
    }
    A(std::string msg1) {
        this->msg1 = msg1;
    }
    void printMsg1() {
        std::cout << msg1 << std::endl;
    }
   
protected:
    std::string msg2 = "2";
private:
    std::string msg3 = "3";
};

class B : public A {
public:
    B():A("Constructor from B") { // <------------- Конкретный конструктор
        std::cout << "B constructor is run!" << std::endl;
    }
    
    void printMsg() {

        msg2 = "this";
        std::cout << msg2;
    }

};



int main()
{
    B b;
    b.printMsg1();

}
