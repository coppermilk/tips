// s90.cpp

#include <iostream>
#include <string>
using namespace std;

class Human;
class Apple;

class Apple {
    friend Human; // <--- class имеет доступ к privat & public касса Apple
private:
    int weight;
    std::string color;
public:
    Apple(int weight, std::string color) {
        this->weight = weight;
        this->color = color;
    }
   
};

class Human {
public:
  
    void EatApple(Apple& apple) {
    cout << "Eat apple " << apple.weight << " gramm" << endl;
    }
};



int main()
{
    Apple apple(150, "Red");
    Human h;
    h.EatApple(apple);
}
