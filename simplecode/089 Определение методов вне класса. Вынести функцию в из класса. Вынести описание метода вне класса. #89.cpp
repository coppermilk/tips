#include <iostream>
#include <string>
class MyClass
{
public:
	MyClass();
	~MyClass();
	void PrintMessage();

private:

};

MyClass::MyClass()
{
}

MyClass::~MyClass()
{
}

void MyClass::PrintMessage() {
	std::cout << "Hello" << std::endl; // <-------- реализация вне класса, нужно если класс длинный
}


int main() {

	MyClass a;
	a.PrintMessage();

    return 0;
}

