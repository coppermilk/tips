// s99.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class A {
public:
    std::string msg1 = "1";
protected:
    std::string msg2 = "2";
private:
    std::string msg3 = "3";
};

class B : public A{
public:
    void printMsg() {
  
    msg2 = "this";
    std::cout << msg2;
}

};

int main()
{
    B b;
    b.printMsg();
    
}

