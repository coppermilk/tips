/******************************************************************************

Welcome to GDB Online.
GDB online is an online compiler and debugger tool for C, C++, Python, Java, PHP, Ruby, Perl,
C#, VB, Swift, Pascal, Fortran, Haskell, Objective-C, Assembly, HTML, CSS, JS, SQLite, Prolog.
Code, Compile, Run and Debug online from anywhere in world.

*******************************************************************************/
#include <iostream>
#define DEBUG 8
int main()
{
#if DEBUG == 9
    std::cout << "full" << std::endl;
#elif DEBUG > 6
    std::cout << "this is sparta" << std::endl;
#else

    for(int i = 0; i< 5; ++i){
        std::cout << i << std::endl;
    }
#endif // DEBUG
    return 0;
}
