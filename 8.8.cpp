#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

void file_it(ostream & os, double fo,
        const double fe[], int n);
const int LIMIT = 5;

int main() {
        ofstream fout;
        const char * fn = "ep_data.txt";
        fout.open(fn);
        if (!fout.is_open()) {
                cout << "Cant open " << fn << " Bye\n";
                exit(EXIT_FAILURE);
        }
        double objective;
        cout << "Input focus distance objective in mm: ";
        cin >> objective;
        double eps[LIMIT];
        cout << "Input focus distance in mm " << LIMIT << " Lens:\n";
        for (int i = 0; i < LIMIT; i++) {
                cout << "Lens #" << i + 1 << ":";
                cin >> eps[i];
        }
        file_it(fout, objective, eps, LIMIT);
        file_it(cout, objective, eps, LIMIT);
        cout << "End";
        return 0;
}

void file_it(ostream & os, double fo,
        const double fe[], int n) {
        ios_base::fmtflags initial;
        auto inital = os.setf(ios_base::fixed);

        os.precision(0);
        os << "focus distance objective: " << fo << " mm" << endl;
        os.setf(ios::showpoint);
        os.precision(1);
        os.width(12);
        os << "coef scale";
        os.width(15);
        os << "lens" << endl;
        for (int i = 0; i < n; i++) {
                os.width(12);
                os << fe[i];
                os.width(15);
                os << int(fo / fe[i] + 0.5) << endl;
        }
        os.setf(initial);
}
