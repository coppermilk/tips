#include <iostream>
#include <cmath>
#include "coordin.h"

polar ect_to_polar(rect xypos){
    using namespace std;
    polar answer;
    answer.distance = sqrt(xypos.x * xypos.x + xypos.y * xypos.y);
    answer.angle = atan2(xypos.y, xypos.x);
    return answer;

}

void show_polar(polar dapos){
    using namespace std;
    const double Rad_to_deg = 57.2957791;
    cout << "radius: " << dapos.distance;
    cout << "angle:" << dapos.angle* Rad_to_deg;
    
}
