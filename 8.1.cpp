// 8.1 inline.cpp
#include <iostream>


// определение встроенонной функции
inline double square(double x) {
        return x * x;
}

int main() {
        using namespace std;
        double a, b;
        double c = 13.0;

        a = square(5.0);
        b = square(4.5 + 7.5);
        cout << "a = " << a << ", b = " << b << ", c = " << c << endl;
        cout << "c = " << square(c);
        return 0;
}
