// 8.3 secref.cpp
#include <iostream>

int main() {
        using namespace std;
        int rats = 101;
        int & rodents = rats;
        cout << "rats = " << rats;
        cout << " rodents = " << rodents << endl;

        cout << "adress rats = " << & rats;
        cout << " adress rodents = " << & rodents << endl;

        int bunnies = 50;
        rodents = bunnies;
        cout << "rats = " << rats;
        cout << " bunnies = " << bunnies;
        cout << " rodents = " << rodents << endl;

        cout << "adress rats = " << & rats;
        cout << " adress bunnies = " << & bunnies;
        cout << " adress rodents = " << & rodents << endl;
        return 0;
}
