﻿#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "opencv2/imgproc/imgproc.hpp"

cv::Mat resizeKeepAspectRatio(const cv::Mat& input, const cv::Size& dstSize, const cv::Scalar& bgcolor);

int main(int argc, char* argv[])
{
    // read an image
    cv::Mat result;
    cv::Mat image1 = cv::imread("bobo.jpg");

    const cv::Size DEFAULT_SIZE(512, 512);
    const cv::Scalar WHITE_COLOR(255, 255, 255);

    result = resizeKeepAspectRatio(image1, DEFAULT_SIZE, WHITE_COLOR);
    cv::imshow("OpenCV Window", result);

    // wait key for 5000 ms
    cv::waitKey(5000);

    return 0;
}

cv::Mat resizeKeepAspectRatio(const cv::Mat& input, const cv::Size& dstSize, const cv::Scalar& bgcolor)
{
    cv::Mat output;

    double h1 = dstSize.width * (input.rows / (double)input.cols);
    double w2 = dstSize.height * (input.cols / (double)input.rows);
    if (h1 <= dstSize.height) {
        cv::resize(input, output, cv::Size(dstSize.width, h1));
    }
    else {
        cv::resize(input, output, cv::Size(w2, dstSize.height));
    }

    int top = (dstSize.height - output.rows) / 2;
    int down = (dstSize.height - output.rows + 1) / 2;
    int left = (dstSize.width - output.cols) / 2;
    int right = (dstSize.width - output.cols + 1) / 2;

    cv::copyMakeBorder(output, output, top, down, left, right, cv::BORDER_CONSTANT, bgcolor);

    return output;
}